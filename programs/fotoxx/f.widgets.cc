/********************************************************************************

   Fotoxx      edit photos and manage collections  

   Copyright 2007-2020 Michael Cornelison
   source code URL: https://kornelix.net
   contact: mkornelix@gmail.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.

*********************************************************************************

   Fotoxx window and menu build functions

   build_widgets           build widgets and menus for F/G/W/M view modes
   m_viewmode              set current F/G/W/M view mode
   popup_menufunc          image/thumb right-click menu func
   image_Rclick_popup      popup menu for image right-click
   gallery_Lclick_func     thumbnail left-click function
   gallery_Rclick_popup    popup menu for thumbnail right-click
   m_favorites             function to generate favorites menu
   favorites_callback      response function for clicked menu

*********************************************************************************/

#define EX extern                                                                //  enable extern declarations
#include "fotoxx.h"                                                              //  (variables in fotoxx.h are defined)

/********************************************************************************/

GtkWidget   *mFile, *mGallery, *mMap, *mMeta, *mArea;
GtkWidget   *mEdit, *mEnhance, *mEffects, *mWarp, *mComb;
GtkWidget   *mProc, *mTools, *mHelp;
GtkWidget   *popmenu_image, *popmenu_raw, *popmenu_video;
GtkWidget   *popmenu_thumb, *popmenu_album;


//  initialize widgets and menus for F/G/W/M view modes
//  called from main() before gtk_main() loop is entered

void build_widgets()
{
   Mwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);                                   //  create main window
   gtk_window_set_title(MWIN,Frelease);

   MWhbox = gtk_box_new(HORIZONTAL,0);                                           //  main window top container
   gtk_container_add(GTK_CONTAINER(Mwin),MWhbox);

   MWmenu = gtk_box_new(VERTICAL,0);                                             //  container for main window menus
   gtk_box_pack_start(GTK_BOX(MWhbox),MWmenu,0,1,0);

   MWvbox = gtk_box_new(VERTICAL,0);                                             //  container for F/G/M/W views
   gtk_box_pack_start(GTK_BOX(MWhbox),MWvbox,1,1,0);

   G_SIGNAL(Mwin,"delete-event",delete_event,0);                                 //  connect signals to main window
   G_SIGNAL(Mwin,"destroy",destroy_event,0);
   G_SIGNAL(Mwin,"window-state-event",state_event,0);
   G_SIGNAL(Mwin,"key-press-event",KBpress,0);                                   //  connect KB events to main window
   G_SIGNAL(Mwin,"key-release-event",KBrelease,0);

   //  F view widgets - image file

   Fhbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Fhbox,1,1,0);
   Fvbox = gtk_box_new(VERTICAL,0);                                              //  vbox for image
   gtk_box_pack_start(GTK_BOX(Fhbox),Fvbox,1,1,0);
   Fpanel = gtk_box_new(HORIZONTAL,0);                                           //  panel over image
   gtk_box_pack_start(GTK_BOX(Fvbox),Fpanel,0,0,0);
   gtk_widget_set_size_request(Fpanel,0,20);
   Fpanlab = gtk_label_new("panel");
   gtk_box_pack_start(GTK_BOX(Fpanel),Fpanlab,0,0,0);
   Fpanelshow = 1;                                                               //  panel normally shows
   Fdrawin = gtk_drawing_area_new();                                             //  image drawing area
   gtk_box_pack_start(GTK_BOX(Fvbox),Fdrawin,1,1,0);
   gtk_widget_hide(Fhbox);

   gtk_widget_add_events(Fdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to image window
   gtk_widget_add_events(Fdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Fdrawin,GDK_BUTTON_MOTION_MASK);
   gtk_widget_add_events(Fdrawin,GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(Fdrawin,GDK_SCROLL_MASK);
   G_SIGNAL(Fdrawin,"button-press-event",mouse_event,0);                         //  connect signals
   G_SIGNAL(Fdrawin,"button-release-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"motion-notify-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"scroll-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"draw",Fpaint,0);
   drag_drop_dest(Fdrawin,drop_event);                                           //  accept drag-drop file

   //  G view widgets - thumbnail gallery

   Ghbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Ghbox,1,1,0);
   Gvbox = gtk_box_new(VERTICAL,0);                                              //  vbox for gallery
   gtk_box_pack_start(GTK_BOX(Ghbox),Gvbox,1,1,0);
   Gpanel = gtk_box_new(HORIZONTAL,0);                                           //  top panel for [TOP] and navi buttons
   gtk_box_pack_start(GTK_BOX(Gvbox),Gpanel,0,0,2);
   Galbum = gtk_button_new_with_label(E2X("Album"));                             //  [Album] button in panel
   gtk_box_pack_start(GTK_BOX(Gpanel),Galbum,0,0,3);
   Gtop = gtk_button_new_with_label(E2X("TOP"));                                 //  [TOP] button in panel
   gtk_box_pack_start(GTK_BOX(Gpanel),Gtop,0,0,3);
   Gsep = gtk_label_new(0);
   gtk_label_set_markup(GTK_LABEL(Gsep),"<span font=\"sans bold 12\" >@</span>");
   gtk_box_pack_start(GTK_BOX(Gpanel),Gsep,0,0,10);

   Gsep = gtk_separator_new(HORIZONTAL);                                         //  separator line
   gtk_box_pack_start(GTK_BOX(Gvbox),Gsep,0,0,3);
   Gscroll = gtk_scrolled_window_new(0,0);                                       //  scrolled window for gallery
   gtk_scrolled_window_set_policy(SCROLLWIN(Gscroll),NEVER,ALWAYS);
   Gadjust = gtk_scrolled_window_get_vadjustment(SCROLLWIN(Gscroll));
   gtk_box_pack_start(GTK_BOX(Gvbox),Gscroll,1,1,0);
   Gdrawin = gtk_drawing_area_new();                                             //  gallery drawing area
   gtk_container_add(GTK_CONTAINER(Gscroll),Gdrawin);
   gtk_widget_hide(Ghbox);

   gtk_widget_add_events(Gdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to gallery window
   gtk_widget_add_events(Gdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Gdrawin,GDK_POINTER_MOTION_MASK);
   G_SIGNAL(Gtop,"clicked",navi::newtop,0);
   G_SIGNAL(Galbum,"clicked",navi::newalbum,0);
   G_SIGNAL(Gdrawin,"button-press-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"button-release-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"motion-notify-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"draw",navi::gallery_paint,null);
   drag_drop_source(Gdrawin,navi::gallery_dragfile);                             //  start file drag-drop
   drag_drop_dest(Gdrawin,navi::gallery_dropfile);                               //  accept drag-drop file

   //  M view widgets - internet maps

   Mhbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Mhbox,1,1,0);
   Mvbox = gtk_box_new(VERTICAL,0);                                              //  vbox for net map window
   gtk_box_pack_start(GTK_BOX(Mhbox),Mvbox,1,1,0);
   gtk_widget_hide(Mhbox);

   //  W view widgets - local map files

   Whbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Whbox,1,1,0);
   Wvbox = gtk_box_new(VERTICAL,0);                                              //  vbox for file map window
   gtk_box_pack_start(GTK_BOX(Whbox),Wvbox,1,1,0);
   Wdrawin = gtk_drawing_area_new();                                             //  filemap drawing area
   gtk_box_pack_start(GTK_BOX(Wvbox),Wdrawin,1,1,0);
   gtk_widget_hide(Whbox);

   gtk_widget_add_events(Wdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to filemap window
   gtk_widget_add_events(Wdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Wdrawin,GDK_BUTTON_MOTION_MASK);
   gtk_widget_add_events(Wdrawin,GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(Wdrawin,GDK_SCROLL_MASK);
   G_SIGNAL(Wdrawin,"button-press-event",mouse_event,0);                         //  connect signals
   G_SIGNAL(Wdrawin,"button-release-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"motion-notify-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"scroll-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"draw",Fpaint,0);

   //  menu popup text (tool tips) ----------------------------------------
   
   //  main menu buttons
   cchar * File_tip = E2X("Rename, copy/move, delete, print");
   cchar * Gallery_tip = E2X("Thumbnails, bookmarks, albums, slide show");
   cchar * Maps_tip = E2X("View images by map location");
   cchar * favorites_tip = E2X("Custom favorites menu");
   cchar * prev_next_tip = E2X("Left/right click: previous/next (also arrow keys)");
   cchar * zoom_menu_tip = E2X("Left/right click: zoom image/thumb size, meta view, list view \n"                    // 20.04
                               "  (keyboard [-] and [+] keys also work)");
   cchar * save_tip = E2X("Save modified file as new version or new file");
   cchar * meta_tip = E2X("Metadata: captions, tags, ratings, geotags, search images");
   cchar * areas_tip = E2X("Select areas to edit separately, save, copy and paste");
   cchar * undo_redo_tip = E2X("Left/right click: undo/redo 1 edit \n"
                               "  with key A: undo/redo all edits \n"
                               "  middle click: go to any prior edit");
   cchar * edit_tip = E2X("Image edit basic functions");
   cchar * enhance_tip = E2X("Image repair and enhance");
   cchar * effects_tip = E2X("Artistic effects (filters)");
   cchar * warp_tip = E2X("Image warp, unwarp, transform");
   cchar * combine_tip = E2X("HDR, HDF, panorama, stack, mashup");
   cchar * process_tip = E2X("Batch processing, custom scripts");
   cchar * tools_tip = E2X("Image index, user preferences, shortcuts, utilities");
   cchar * help_tip = E2X("User Guide, recent changes, log file, about");

   //  file menu
   cchar * Fview_tip = E2X("Current File (R-click or key F)");
   cchar * new_session_tip = E2X("Open a parallel Fotoxx session");
   cchar * cycle2files_tip = E2X("Cycle 2 Prior Files");
   cchar * cycle3files_tip = E2X("Cycle 3 Prior Files");
   cchar * view360_tip = E2X("View a 360 degree panorama image file");
   cchar * rename_tip = E2X("Change file name");
   cchar * permissions_tip = E2X("View and change file permissions");            //  20.0
   cchar * blank_image_tip = E2X("Create a blank image");
   cchar * blank_window_tip = E2X("Toggle - blank or restore window");
   cchar * copy_move_tip = E2X("Copy or Move file to new location");
   cchar * copyto_desktop_tip = E2X("Copy file to the desktop");
   cchar * copyto_clipboard_tip = E2X("Copy file to the clipboard");
   cchar * set_wallpaper_tip = E2X("Set file as desktop wallpaper (GNOME)");
   cchar * show_on_net_map_tip = E2X("Show location on Internet map");
   cchar * deltrash_tip = E2X("Delete or trash file");
   cchar * print_tip = E2X("Print the current image");
   cchar * print_calibrated_tip = E2X("Print current image with adjusted colors");
   cchar * quit_tip = E2X("Quit Fotoxx");

   //  gallery menu
   cchar * Gview_tip = E2X("Thumbnail Gallery (R-click or key G)");
   cchar * thumbview_tip = E2X("Gallery view with thumbnails and file data");                                       //  20.04
   cchar * metaview_tip = E2X("Gallery view with thumbnails and basic metadata");
   cchar * listview_tip = E2X("Gallery view with small thumbnails and file names");
   cchar * recentfiles_tip = E2X("Gallery of recently viewed image files");
   cchar * newfiles_tip = E2X("Gallery of newest image files");
   cchar * jump_begin_tip = E2X("Jump to beginning [home]");
   cchar * jump_end_tip = E2X("Jump to end [end]");
   cchar * source_folder_tip = E2X("Set gallery from current image file");
   cchar * sort_order_tip = E2X("Change sort order");
   cchar * alldirs_tip = E2X("List all folders, click any for gallery view");
   cchar * select_files_tip = E2X("select input files for album, batch, script functions");
   cchar * bookmarks_tip = E2X("Set and recall bookmarked image locations");
   cchar * manage_albums_tip = E2X("Organize images into albums");
   cchar * update_albums_tip = E2X("Update albums for new file versions");
   cchar * album_mass_update_tip = E2X("Mass update album files");
   cchar * gallery2album_tip = E2X("Save current gallery as album");
   cchar * slideshow_tip = E2X("Start a slide show");
   
   //  map menu
   cchar * map_view_tip = E2X("Maps (R-click or key M)");
   cchar * net_map_tip = E2X("Open Internet map");
   cchar * net_source_tip = E2X("Choose Internet map source");
   cchar * net_locs_tip = E2X("Internet map locations");
   cchar * file_map_tip = E2X("Open file map");
   cchar * choose_map_tip = E2X("Choose file map");
   cchar * markers_tip = E2X("Set map markers for all images or current gallery");

   //  metadata menu
   cchar * meta_view_main_tip = E2X("List a few key metadata items");
   cchar * meta_view_all_tip = E2X("List all metadata items");
   cchar * meta_edit_main_tip = E2X("Edit image tags/geotags/caption/rating ...");
   cchar * meta_manage_tags_tip = E2X("Define tags (keywords) used for searching images");
   cchar * meta_edit_any_tip = E2X("Edit any image metadata");
   cchar * meta_delete_tip = E2X("Remove selected image metadata");
   cchar * meta_captions_tip = E2X("Show file name, captions, comments");
   cchar * meta_places_dates_tip = E2X("Find all images for a location [date]");
   cchar * meta_timeline_tip = E2X("Show image counts by month, select and report");
   cchar * search_images_tip = E2X("Find images meeting search criteria");

   //  select area menu
   cchar * select_tip = E2X("Select object or area for editing");
   cchar * select_hairy_tip = E2X("Select hairy or irregular edge");
   cchar * select_find_gap_tip = E2X("Find a gap in an area outline");
   cchar * select_show_tip = E2X("Show (outline) existing area");
   cchar * select_hide_tip = E2X("Hide existing area");
   cchar * select_enable_tip = E2X("Enable area for editing");
   cchar * select_disable_tip = E2X("Disable area for editing");
   cchar * select_invert_tip = E2X("Reverse existing area");
   cchar * select_clear_tip = E2X("Erase existing area");
   cchar * select_copy_tip = E2X("Copy area for later pasting into image");
   cchar * select_paste_tip = E2X("Paste previously copied area into image");
   cchar * select_load_tip = E2X("Open a file and paste as area into image");
   cchar * select_save_tip = E2X("Save area to a file with transparency");

   //  edit menu
   cchar * trim_rotate_tip = E2X("Trim/Crop margins and/or Rotate");
   cchar * upright_tip = E2X("Auto upright a rotated image based on EXIF data");
   cchar * retouch_tip = E2X("Adjust brightness, contrast, color");
   cchar * resize_tip = E2X("Change pixel dimensions");
   cchar * adjust_RGB_tip = E2X("Adjust color using RGB or CMY colors");
   cchar * adjust_HSL_tip = E2X("Adjust color using HSL colors");
   cchar * markup_tip = E2X("Draw on image: text, line/arrow, box, ellipse");
   cchar * paint_image_tip = E2X("Paint image pixels using the mouse");
   cchar * copy_pixels1_tip = E2X("Copy pixels within an image using the mouse");
   cchar * copy_pixels2_tip = E2X("Copy pixels from one image to another using the mouse");
   cchar * paint_edits_tip = E2X("Paint edit function gradually with mouse");
   cchar * undo_edits_tip = E2X("Incrementally undo prior edits gradually with mouse");
   cchar * plugins_tip = E2X("Edit plugins menu or run a plugin function");
   cchar * raw_therapee_tip = E2X("Specialized program for editing RAW files");

   //  enhance menu
   cchar * voodoo1_tip = E2X("Fast auto enhance that may work OK");
   cchar * voodoo2_tip = E2X("Fast auto enhance that may work OK");
   cchar * brite_dist_tip = E2X("Edit brightness distribution");
   cchar * gradients_tip = E2X("Magnify brightness gradients to enhance details");
   cchar * flatten_tip = E2X("Flatten brightness distribution to enhance details");
   cchar * retinex_tip = E2X("Rescale RGB - reduce color caste and fog/haze");
   cchar * sharpen_tip = E2X("Make the image look sharper");
   cchar * blur_tip = E2X("Blur the image, different methods");
   cchar * denoise_tip = E2X("Filter noise from low-light photos");
   cchar * redeyes_tip = E2X("Fix red-eyes from electronic flash");
   cchar * match_colors_tip = E2X("Match colors on one image with another");
   cchar * smart_erase_tip = E2X("Remove unwanted objects");
   cchar * chromatic1_tip = E2X("Fix color fringes in outer areas of an image");
   cchar * chromatic2_tip = E2X("Fix color band on dark/bright feature edges");
   cchar * vignette_tip = E2X("Change brightness or color radially");
   cchar * remove_dust_tip = E2X("Remove dust spots from scanned slides");

   //  Effects menu
   cchar * sketch_tip = E2X("Convert to simulated sketch");
   cchar * cartoon_tip = E2X("Convert image into a cartoon drawing");
   cchar * line_drawing_tip = E2X("Convert to line drawing (edge detection)");
   cchar * emboss_tip = E2X("Create an embossed or 3D appearance");
   cchar * tiles_tip = E2X("Convert to square tiles");
   cchar * dither_tip = E2X("Convert to dithered dots");
   cchar * painting_tip = E2X("Convert into a simulated painting");
   cchar * texture_tip = E2X("Add texture to an image");
   cchar * pattern_tip = E2X("Tile image with a repeating pattern");
   cchar * mosaic_tip = E2X("Create a mosaic with tiles made from all images");
   cchar * color_mode_tip = E2X("Make BW/color, negative/positive, sepia");
   cchar * color_depth_tip = E2X("Reduce color depth (posterize)");
   cchar * shift_colors_tip = E2X("Shift/convert colors into other colors");
   cchar * alien_colors_tip = E2X("Change color hue using an algorithm");
   cchar * brite_ramp_tip = E2X("Add a brightness/color ramp across the image");
   cchar * paint_transp_tip = E2X("Paint image transparency using the mouse");
   cchar * mirror_tip = E2X("Mirror image horizontally or vertically");
   cchar * anykernel_tip = E2X("Process an image using a custom kernel");

   //  warp menu
   cchar * unbend_tip = E2X("Remove curvature, esp. panoramas");
   cchar * perspective_tip = E2X("Straighten objects seen from an angle");
   cchar * warp_area_tip = E2X("Distort image areas using the mouse");
   cchar * unwarp_closeup_tip = E2X("Unwarp closeup face photo to remove distortion");
   cchar * warp_curved_tip = E2X("Distort the whole image using the mouse");
   cchar * warp_linear_tip = E2X("Distort the whole image using the mouse");
   cchar * warp_affine_tip = E2X("Distort the whole image using the mouse");
   cchar * flatbook_tip = E2X("Flatten a photographed book page");
   cchar * area_rescale_tip = E2X("Rescale image outside selected areas");
   cchar * waves_tip = E2X("Warp an image with a wave pattern");
   cchar * twist_tip = E2X("Twist image centered at mouse position");
   cchar * sphere_tip = E2X("Make a spherical projection of an image");
   cchar * stretch_tip = E2X("Image scale increases from center to edge");
   cchar * inside_out_tip = E2X("Turn an image inside-out");
   cchar * tiny_planet_tip = E2X("Convert an image into a Tiny Planet");
   cchar * escher_spiral_tip = E2X("Generate an inward spiraling recursive image");

   //  combine menu
   cchar * HDR_tip = E2X("Combine bright/dark images for better detail");
   cchar * HDF_tip = E2X("Combine near/far focus images for deeper focus");
   cchar * stack_paint_tip = E2X("Combine images to erase passing people, etc.");
   cchar * stack_noise_tip = E2X("Combine noisy images into a low-noise image");
   cchar * stack_layer_tip = E2X("Combine image layers, mouse select and expose");
   cchar * stack_slider_tip = E2X("Compare two images separated by sliding boundary");
   cchar * image_diffs_tip = E2X("Show differences between two images");
   cchar * pano_horz_tip = E2X("Combine images into a panorama");
   cchar * pano_vert_tip = E2X("Combine images into a vertical panorama");
   cchar * pano_PT_tip = E2X("Combine images into a panorama (panorama tools)");
   cchar * montage_tip = E2X("Combine images into a montage of images");
   cchar * mashup_tip = E2X("Arrange images and text in a layout (montage)");

   //  process menu
   cchar * batch_convert_tip = E2X("Rename/convert/resize/move multiple files");
   cchar * batch_upright_tip = E2X("Upright multiple rotated image files");
   cchar * batch_deltrash_tip = E2X("Delete or Trash multiple files");
   cchar * batch_RAW_tip = E2X("Convert camera RAW files to tiff/png/jpeg");
   cchar * burn_DVD_tip = E2X("Burn selected image files to DVD/BlueRay disc");
   cchar * export_filelist_tip = E2X("Create a file of selected image files");
   cchar * export_files_tip = E2X("Export selected image files to a folder");
   cchar * batch_tags_tip = E2X("Add/remove tags for multiple images");
   cchar * batch_rename_tags_tip = E2X("Convert tag names for all images");
   cchar * batch_photo_DT_tip = E2X("change or shift photo dates/times");
   cchar * batch_change_mdata_tip = E2X("Add/change/delete metadata for multiple images");
   cchar * batch_report_mdata_tip = E2X("Report metadata for multiple images");
   cchar * batch_geotags_tip = E2X("Add/revise geotags for multiple images");
   cchar * edit_script_tip = E2X("Build a custom script with multiple edit functions");
   cchar * run_script_tip = E2X("Run custom script to edit the current image file");
   cchar * batch_script_tip = E2X("Run custom script to edit a batch of image files");

   //  tools menu
   cchar * index_tip = E2X("Index new files and make thumbnails");
   cchar * quick_index_tip = E2X("Quick incremental index update");
   cchar * move_fotoxx_home_tip = E2X("Move Fotoxx home folder");
   cchar * preferences_tip = E2X("User preferences and settings");
   cchar * KBshortcuts_tip = E2X("Keyboard Shortcuts");
   cchar * RGB_dist_tip = E2X("Show RGB brightness distribution");
   cchar * magnify_tip = E2X("Magnify image around the mouse position");
   cchar * duplicates_tip = E2X("Search all image files and report duplicates");
   cchar * show_RGB_tip = E2X("Show RGB colors at mouse click");
   cchar * color_profile_tip = E2X("Convert to another color profile");
   cchar * calib_printer_tip = E2X("Calibrate printer colors");
   cchar * gridlines_tip = E2X("Show or revise grid lines");
   cchar * line_color_tip = E2X("Change color of foreground lines");
   cchar * darkbrite_tip = E2X("Highlight darkest and brightest pixels");
   cchar * map_pixel_bias_tip = E2X("map raw pixel bias (camera sensor, vignette)");
   cchar * map_dead_pixels_tip = E2X("map raw dead pixels (camera sensor)");
   cchar * monitor_color_tip = E2X("Chart to adjust monitor color");
   cchar * monitor_gamma_tip = E2X("Chart to adjust monitor gamma");
   cchar * change_lang_tip = E2X("Change the GUI language");
   cchar * untranslated_tip = E2X("Report missing translations");
   cchar * phone_home_permit_tip = E2X("Permit or decline anonymous usage statistics"); 
   cchar * resources_tip = E2X("Memory and CPU (to terminal/logfile)");
   cchar * appimage_files_tip = E2X("List files included in appimage container");
   cchar * zappcrash_test_tip = E2X("test crash report with source line numbers");

   //  help menu
   cchar * user_guide_tip = E2X("Read the user guide");
   cchar * recent_changes_tip = E2X("Recent user guide changes");
   cchar * edit_overview_tip = E2X("Overview of all edit functions");            //  20.0
   cchar * changelog_tip = E2X("List updates by Fotoxx version");
   cchar * logfile_tip = E2X("View the log file and error messages");
   cchar * manpage_tip = E2X("Fotoxx Man Page - summary of capabilities");       //  20.0
   cchar * command_params_tip = E2X("List command line parameters");
   cchar * translations_tip = E2X("How to do Fotoxx translations");
   cchar * homepage_tip = E2X("Show the Fotoxx web page");
   cchar * license_tip = E2X("Fotoxx license - terms of use");                   //  19.0
   cchar * privacy_tip = E2X("Fotoxx privacy policy");                           //  19.1
   cchar * about_tip = E2X("Version, contact, credits");


   //  build menu table ---------------------------------------------------------

   #define MENU(_topmenu, _text, _icon, _desc, _func, _arg)       \
      me = Nmenus++;                                              \
      if (me >= maxmenus) zappcrash("maxmenus exceeded");         \
      menutab[me].topmenu = _topmenu;                             \
      menutab[me].menu = _text;                                   \
      menutab[me].icon = _icon;                                   \
      menutab[me].desc = _desc;                                   \
      menutab[me].func = _func;                                   \
      if (_arg) menutab[me].arg = _arg;                           \
      else menutab[me].arg = _text;                               \

   int      me;
   Nmenus = 0;

   mFile = create_popmenu();
   MENU(mFile,    E2X("File View F"), 0,           Fview_tip,                    m_viewmode, "F" );
   MENU(mFile,    E2X("New Session"), 0,           new_session_tip,              m_new_session, 0 );
   MENU(mFile,    E2X("Source Folder"), 0,         source_folder_tip,            m_source_folder, 0 );
   MENU(mFile,    E2X("Cycle 2"), 0,               cycle2files_tip,              m_cycle2files, 0 );
   MENU(mFile,    E2X("Cycle 3"), 0,               cycle3files_tip,              m_cycle3files, 0 );
   MENU(mFile,    E2X("View 360° Pano"), 0,        view360_tip,                  m_view360, 0);
   MENU(mFile,    E2X("Rename"), 0,                rename_tip,                   m_rename, 0 );
   MENU(mFile,    E2X("Permissions"), 0,           permissions_tip,              m_permissions, 0 );                //  20.0
   MENU(mFile,    E2X("Blank Image"), 0,           blank_image_tip,              m_blank_image, 0 );
   MENU(mFile,    E2X("Blank Window"), 0,          blank_window_tip,             m_blank_window, 0 );
   MENU(mFile,    E2X("Copy/Move"), 0,             copy_move_tip,                m_copy_move, 0 );
   MENU(mFile,    E2X("Copy to Desktop"), 0,       copyto_desktop_tip,           m_copyto_desktop, 0 );
   MENU(mFile,    E2X("Copy to Clipboard"), 0,     copyto_clipboard_tip,         m_copyto_clip, 0 );
   MENU(mFile,    E2X("Set Wallpaper"), 0,         set_wallpaper_tip,            m_wallpaper, 0 );
   MENU(mFile,    E2X("Show on Map"), 0,           show_on_net_map_tip,          m_netmap_zoomin, 0 );
   MENU(mFile,    E2X("Delete/Trash"), 0,          deltrash_tip,                 m_delete_trash, 0 );
   MENU(mFile,    E2X("Print"), 0,                 print_tip,                    m_print, 0 );
   MENU(mFile,    E2X("Print Calibrated"), 0,      print_calibrated_tip,         m_print_calibrated, 0 );
   MENU(mFile,    E2X("Quit"), 0,                  quit_tip,                     m_quit, 0 );

   mGallery = create_popmenu();
   MENU(mGallery,   E2X("Gallery View G"), 0,      Gview_tip,                    m_viewmode, "G" );
   MENU(mGallery,   E2X("Thumb View"), 0,          thumbview_tip,                m_thumbview, 0 );                  //  20.04
   MENU(mGallery,   E2X("Meta View"), 0,           metaview_tip,                 m_metaview, 0 );
   MENU(mGallery,   E2X("List View"), 0,           listview_tip,                 m_listview, 0 );
   MENU(mGallery,   E2X("Recent"), 0,              recentfiles_tip,              m_recentfiles, 0 );
   MENU(mGallery,   E2X("Newest"), 0,              newfiles_tip,                 m_newfiles, 0 );
   MENU(mGallery,   E2X("GoTo First"), 0,          jump_begin_tip,               navi::menufuncx, "First" );
   MENU(mGallery,   E2X("GoTo Last"), 0,           jump_end_tip,                 navi::menufuncx, "Last" );
   MENU(mGallery,   E2X("Source Folder"), 0,       source_folder_tip,            m_source_folder, 0 );
   MENU(mGallery,   E2X("Sort Gallery"), 0,        sort_order_tip,               navi::menufuncx, "Sort" );
   MENU(mGallery,   E2X("All Folders"), 0,         alldirs_tip,                  m_alldirs, 0 );
   MENU(mGallery,   E2X("Select Files"), 0,        select_files_tip,             m_select_files, 0 );
   MENU(mGallery,   E2X("Bookmarks"), 0,           bookmarks_tip,                m_bookmarks, 0 );
   MENU(mGallery,   E2X("Manage Albums"), 0,       manage_albums_tip,            m_manage_albums, 0 );
   MENU(mGallery,   E2X("Update Albums"), 0,       update_albums_tip,            m_update_albums, 0 );
   MENU(mGallery,   E2X("Album Mass Update"), 0,   album_mass_update_tip,        m_album_mass_update, 0 );
   MENU(mGallery,   E2X("Gallery to Album"), 0,    gallery2album_tip,            m_gallery2album, 0 );
   MENU(mGallery,   E2X("Slide Show"), 0,          slideshow_tip,                m_slideshow, 0 );

   mMap = create_popmenu();
   MENU(mMap,     E2X("Map View M"), 0,            map_view_tip,                 m_viewmode, "?" );    //  prior view   20.0
   MENU(mMap,     E2X("Net Map"), 0,               net_map_tip,                  m_viewmode, "M");
   MENU(mMap,     E2X("Net Source"), 0,            net_source_tip,               m_netmap_source, 0 );
   MENU(mMap,     E2X("Net Locs"), 0,              net_locs_tip,                 m_netmap_locs, 0 );
   MENU(mMap,     E2X("File Map"), 0,              file_map_tip,                 m_viewmode, "W" );
   MENU(mMap,     E2X("Choose Map"), 0,            choose_map_tip,               m_load_filemap, 0 );
   MENU(mMap,     E2X("Markers"), 0,               markers_tip,                  m_set_map_markers, 0 );

   mMeta = create_popmenu();
   MENU(mMeta,    E2X("View Meta"), 0,             meta_view_main_tip,           m_meta_view_short, 0 );
   MENU(mMeta,    E2X("View All Meta"), 0,         meta_view_all_tip,            m_meta_view_long, 0 );
   MENU(mMeta,    E2X("Edit Meta"), 0,             meta_edit_main_tip,           m_meta_edit_main, 0 );
   MENU(mMeta,    E2X("Manage Tags"), 0,           meta_manage_tags_tip,         m_meta_manage_tags, 0 );
   MENU(mMeta,    E2X("Edit Any Meta"), 0,         meta_edit_any_tip,            m_meta_edit_any, 0 );
   MENU(mMeta,    E2X("Delete Meta"), 0,           meta_delete_tip,              m_meta_delete, 0 );
   MENU(mMeta,    E2X("Captions"), 0,              meta_captions_tip,            m_meta_captions, 0 );
   MENU(mMeta,    E2X("Places/Dates"), 0,          meta_places_dates_tip,        m_meta_places_dates, 0 );
   MENU(mMeta,    E2X("Timeline"), 0,              meta_timeline_tip,            m_meta_timeline, 0 );
   MENU(mMeta,    E2X("Search"), 0,                search_images_tip,            m_search_images, 0 );

   mArea = create_popmenu();
   MENU(mArea,    E2X("Select"), 0,                select_tip,                   m_select, 0 );
   MENU(mArea,    E2X("Select Hairy"), 0,          select_hairy_tip,             m_select_hairy, 0);
   MENU(mArea,    E2X("Find Gap"), 0,              select_find_gap_tip,          m_select_find_gap, 0 );
   MENU(mArea,    E2X("Show"), 0,                  select_show_tip,              m_select_show, 0 );
   MENU(mArea,    E2X("Hide"), 0,                  select_hide_tip,              m_select_hide, 0 );
   MENU(mArea,    E2X("Enable"), 0,                select_enable_tip,            m_select_enable, 0 );
   MENU(mArea,    E2X("Disable"), 0,               select_disable_tip,           m_select_disable, 0 );
   MENU(mArea,    E2X("Invert"), 0,                select_invert_tip,            m_select_invert, 0 );
   MENU(mArea,    E2X("Clear"), 0,                 select_clear_tip,             m_select_clear, 0 );
   MENU(mArea,    E2X("Copy"), 0,                  select_copy_tip,              m_select_copy, 0 );
   MENU(mArea,    E2X("Paste"), 0,                 select_paste_tip,             m_select_paste, 0 );
   MENU(mArea,    E2X("Load"), 0,                  select_load_tip,              m_select_load, 0 );
   MENU(mArea,    E2X("Save"), 0,                  select_save_tip,              m_select_save, 0 );

   mEdit = create_popmenu(); 
   MENU(mEdit,    E2X("Trim/Rotate"), 0,           trim_rotate_tip,              m_trim_rotate, 0 );
   MENU(mEdit,    E2X("Upright"), 0,               upright_tip,                  m_upright, 0 );
   MENU(mEdit,    E2X("Retouch"), 0,               retouch_tip,                  m_retouch, 0 );
   MENU(mEdit,    E2X("Resize"), 0,                resize_tip,                   m_resize, 0 );
   MENU(mEdit,    E2X("Adjust RGB"), 0,            adjust_RGB_tip,               m_adjust_RGB, 0 );
   MENU(mEdit,    E2X("Adjust HSL"), 0,            adjust_HSL_tip,               m_adjust_HSL, 0 );
   MENU(mEdit,    E2X("Markup"), 0,                markup_tip,                   m_markup, 0 );                     //  20.0
   MENU(mEdit,    E2X("Paint Image"), 0,           paint_image_tip,              m_paint_image, 0 );
   MENU(mEdit,    E2X("Copy Pixels 1"), 0,         copy_pixels1_tip,             m_copypixels1, 0 );
   MENU(mEdit,    E2X("Copy Pixels 2"), 0,         copy_pixels2_tip,             m_copypixels2, 0 );
   MENU(mEdit,    E2X("Paint Edits"), 0,           paint_edits_tip,              m_paint_edits, 0 );
   MENU(mEdit,    E2X("Undo Edits"), 0,            undo_edits_tip,               m_undo_edits, 0 );
   MENU(mEdit,    E2X("Plugins"), 0,               plugins_tip,                  m_plugins, 0);
   MENU(mEdit,    E2X("Raw Therapee"), 0,          raw_therapee_tip,             m_rawtherapee, 0);

   mEnhance = create_popmenu();
   MENU(mEnhance,   E2X("Voodoo 1"), 0,            voodoo1_tip,                  m_voodoo1, 0 );
   MENU(mEnhance,   E2X("Voodoo 2"), 0,            voodoo2_tip,                  m_voodoo2, 0);
   MENU(mEnhance,   E2X("Brite Dist"), 0,          brite_dist_tip,               m_brite_dist, 0 );
   MENU(mEnhance,   E2X("Gradients"), 0,           gradients_tip,                m_gradients, 0 );
   MENU(mEnhance,   E2X("Flatten"), 0,             flatten_tip,                  m_flatten, 0 );
   MENU(mEnhance,   E2X("Global Retx"), 0,         retinex_tip,                  m_gretinex, 0 );
   MENU(mEnhance,   E2X("Zonal Retx"), 0,          retinex_tip,                  m_zretinex, 0 );
   MENU(mEnhance,   E2X("Sharpen"), 0,             sharpen_tip,                  m_sharpen, 0 );
   MENU(mEnhance,   E2X("Blur"), 0,                blur_tip,                     m_blur, 0 );
   MENU(mEnhance,   E2X("Denoise"), 0,             denoise_tip,                  m_denoise, 0 );
   MENU(mEnhance,   E2X("Red Eyes"), 0,            redeyes_tip,                  m_redeyes, 0 );
   MENU(mEnhance,   E2X("Match Colors"), 0,        match_colors_tip,             m_match_colors, 0 );
   MENU(mEnhance,   E2X("Smart Erase"), 0,         smart_erase_tip,              m_smart_erase, 0 );
   MENU(mEnhance,   E2X("Chromatic1"), 0,          chromatic1_tip,               m_chromatic1, 0 );                 //  20.0
   MENU(mEnhance,   E2X("Chromatic2"), 0,          chromatic2_tip,               m_chromatic2, 0 );                 //  20.0
   MENU(mEnhance,   E2X("Vignette"), 0,            vignette_tip,                 m_vignette, 0 );
   MENU(mEnhance,   E2X("Remove Dust"), 0,         remove_dust_tip,              m_remove_dust, 0 );

   mEffects = create_popmenu();
   MENU(mEffects,    E2X("Sketch"), 0,                sketch_tip,                m_sketch, 0 );
   MENU(mEffects,    E2X("Cartoon"), 0,               cartoon_tip,               m_cartoon, 0 );
   MENU(mEffects,    E2X("Line Drawing"), 0,          line_drawing_tip,          m_line_drawing, 0 );
   MENU(mEffects,    E2X("Emboss"), 0,                emboss_tip,                m_emboss, 0 );
   MENU(mEffects,    E2X("Tiles"), 0,                 tiles_tip,                 m_tiles, 0 );
   MENU(mEffects,    E2X("Dither"), 0,                dither_tip,                m_dither, 0 );
   MENU(mEffects,    E2X("Painting"), 0,              painting_tip,              m_painting, 0 );
   MENU(mEffects,    E2X("Texture"), 0,               texture_tip,               m_texture, 0 );
   MENU(mEffects,    E2X("Pattern"), 0,               pattern_tip,               m_pattern, 0 );
   MENU(mEffects,    E2X("Mosaic"), 0,                mosaic_tip,                m_mosaic, 0 );
   MENU(mEffects,    E2X("Color Mode"), 0,            color_mode_tip,            m_color_mode, 0 );
   MENU(mEffects,    E2X("Color Depth"), 0,           color_depth_tip,           m_color_depth, 0 );
   MENU(mEffects,    E2X("Shift Colors"), 0,          shift_colors_tip,          m_shift_colors, 0 );
   MENU(mEffects,    E2X("Alien Colors"), 0,          alien_colors_tip,          m_alien_colors, 0 ); 
   MENU(mEffects,    E2X("Brite Ramp"), 0,            brite_ramp_tip,            m_brite_ramp, 0 );
   MENU(mEffects,    E2X("Paint Transp"), 0,          paint_transp_tip,          m_paint_transp, 0 );
   MENU(mEffects,    E2X("Mirror"), 0,                mirror_tip,                m_mirror, 0 );
   MENU(mEffects,    E2X("Custom Kernel"), 0,         anykernel_tip,             m_anykernel, 0 );

   mWarp = create_popmenu();
   MENU(mWarp,    E2X("Unbend"), 0,                   unbend_tip,                m_unbend, 0 );
   MENU(mWarp,    E2X("Perspective"), 0,              perspective_tip,           m_perspective, 0 );
   MENU(mWarp,    E2X("Warp area"), 0,                warp_area_tip,             m_warp_area, 0 );
   MENU(mWarp,    E2X("Warp curved"), 0,              warp_curved_tip,           m_warp_curved, 0 );
   MENU(mWarp,    E2X("Warp linear"), 0,              warp_linear_tip,           m_warp_linear, 0 );
   MENU(mWarp,    E2X("Warp affine"), 0,              warp_affine_tip,           m_warp_affine, 0 );
   MENU(mWarp,    E2X("Unwarp Closeup"), 0,           unwarp_closeup_tip,        m_unwarp_closeup, 0 );
   MENU(mWarp,    E2X("Flatten Book"), 0,             flatbook_tip,              m_flatbook, 0 );
   MENU(mWarp,    E2X("Area Rescale"), 0,             area_rescale_tip,          m_area_rescale, 0);
   MENU(mWarp,    E2X("Make Waves"), 0,               waves_tip,                 m_waves, 0);
   MENU(mWarp,    E2X("Twist"), 0,                    twist_tip,                 m_twist, 0);
   MENU(mWarp,    E2X("Sphere"), 0,                   sphere_tip,                m_sphere, 0);
   MENU(mWarp,    E2X("Stretch"), 0,                  stretch_tip,               m_stretch, 0);
   MENU(mWarp,    E2X("Inside-out"), 0,               inside_out_tip,            m_inside_out, 0);
   MENU(mWarp,    E2X("Tiny Planet"), 0,              tiny_planet_tip,           m_tiny_planet, 0);
   MENU(mWarp,    E2X("Escher Spiral"), 0,            escher_spiral_tip,         m_escher_spiral, 0);               //  20.0

   mComb = create_popmenu();
   MENU(mComb,        "HDR", 0,                       HDR_tip,                   m_HDR, 0 );
   MENU(mComb,        "HDF", 0,                       HDF_tip,                   m_HDF, 0 );
   MENU(mComb,    E2X("Stack/Paint"), 0,              stack_paint_tip,           m_stack_paint, 0 );
   MENU(mComb,    E2X("Stack/Noise"), 0,              stack_noise_tip,           m_stack_noise, 0 );
   MENU(mComb,    E2X("Stack/Layer"), 0,              stack_layer_tip,           m_stack_layer, 0 );
   MENU(mComb,    E2X("Stack/Slider"), 0,             stack_slider_tip,          m_stack_slider, 0 );               //  20.0
   MENU(mComb,    E2X("Image Diffs"), 0,              image_diffs_tip,           m_image_diffs, 0);
   MENU(mComb,    E2X("Panorama"), 0,                 pano_horz_tip,             m_pano_horz, 0 );
   MENU(mComb,    E2X("V. Panorama"), 0,              pano_vert_tip,             m_pano_vert, 0 );
   MENU(mComb,    E2X("PT Panorama"), 0,              pano_PT_tip,               m_pano_PT, 0 );
   MENU(mComb,    E2X("Mashup"), 0,                   mashup_tip,                m_mashup, 0 );
   MENU(mComb,    E2X("Montage"), 0,                  montage_tip,               m_montage, 0 );

   mProc = create_popmenu();
   MENU(mProc,    E2X("Batch Convert"), 0,            batch_convert_tip,         m_batch_convert, 0 );
   MENU(mProc,    E2X("Batch Upright"), 0,            batch_upright_tip,         m_batch_upright, 0 );
   MENU(mProc,    E2X("Batch Delete/Trash"), 0,       batch_deltrash_tip,        m_batch_deltrash, 0 );
   MENU(mProc,    E2X("Batch RAW"), 0,                batch_RAW_tip,             m_batch_RAW, 0 );
   MENU(mProc,    E2X("Burn DVD/BlueRay"), 0,         burn_DVD_tip,              m_burn_DVD, 0 );
   MENU(mProc,    E2X("Export File List"), 0,         export_filelist_tip,       m_export_filelist, 0 );
   MENU(mProc,    E2X("Export Files"), 0,             export_files_tip,          m_export_files, 0 );
   MENU(mProc,    E2X("Batch Tags"), 0,               batch_tags_tip,            m_batch_tags, 0 );
   MENU(mProc,    E2X("Batch Rename Tags"), 0,        batch_rename_tags_tip,     m_batch_rename_tags, 0 );
   MENU(mProc,    E2X("Batch Photo Date"), 0,         batch_photo_DT_tip,        m_batch_photo_date_time, 0 );
   MENU(mProc,    E2X("Batch Change Meta"), 0,        batch_change_mdata_tip,    m_batch_change_metadata, 0 );
   MENU(mProc,    E2X("Batch Report Meta"), 0,        batch_report_mdata_tip,    m_batch_report_metadata, 0 );
   MENU(mProc,    E2X("Batch Geotags"), 0,            batch_geotags_tip,         m_batch_geotags, 0 );
   MENU(mProc,    E2X("Edit Script"), 0,              edit_script_tip,           m_edit_script, 0 );
   MENU(mProc,    E2X("Run Script"), 0,               run_script_tip,            m_run_script, 0);
   MENU(mProc,    E2X("Batch Script"), 0,             batch_script_tip,          m_batch_script, 0);

   mTools = create_popmenu();
   MENU(mTools,   E2X("Index Files"), 0,              index_tip,                 m_index, 0 );
   MENU(mTools,   E2X("Quick Index"), 0,              quick_index_tip,           m_quick_index, 0 );                //  20.0
   MENU(mTools,   E2X("Move Fotoxx Home"), 0,         move_fotoxx_home_tip,      m_move_fotoxx_home, 0 );
   MENU(mTools,   E2X("Preferences"), 0,              preferences_tip,           m_preferences, 0 );
   MENU(mTools,   E2X("KB Shortcuts"), 0,             KBshortcuts_tip,           m_KBshortcuts, 0 );
   MENU(mTools,   E2X("RGB Distribution"), 0,         RGB_dist_tip,              m_RGB_dist, 0 );
   MENU(mTools,   E2X("Magnify Image"), 0,            magnify_tip,               m_magnify, 0 );
   MENU(mTools,   E2X("Find Duplicates"), 0,          duplicates_tip,            m_duplicates, 0 );
   MENU(mTools,   E2X("Show RGB"), 0,                 show_RGB_tip,              m_show_RGB, 0 );
   MENU(mTools,   E2X("Color Profile"), 0,            color_profile_tip,         m_color_profile, 0 );
   MENU(mTools,   E2X("Calibrate Printer"), 0,        calib_printer_tip,         m_calibrate_printer, 0 );
   MENU(mTools,   E2X("Grid Lines"), 0,               gridlines_tip,             m_gridlines, 0 );
   MENU(mTools,   E2X("Line Color"), 0,               line_color_tip,            m_line_color, 0 );
   MENU(mTools,   E2X("Dark/Bright Pixels"), 0,       darkbrite_tip,             m_darkbrite, 0 );
   MENU(mTools,   E2X("Map Pixel Bias"), 0,           map_pixel_bias_tip,        m_map_pixel_bias, 0 );             //  20.0
   MENU(mTools,   E2X("Map Dead Pixels"), 0,          map_dead_pixels_tip,       m_map_dead_pixels, 0 );            //  20.0
   MENU(mTools,   E2X("Monitor Color"), 0,            monitor_color_tip,         m_monitor_color, 0 );
   MENU(mTools,   E2X("Monitor Gamma"), 0,            monitor_gamma_tip,         m_monitor_gamma, 0 );
   MENU(mTools,   E2X("Change Language"), 0,          change_lang_tip,           m_change_lang, 0 );
   MENU(mTools,   E2X("Missing Translations"), 0,     untranslated_tip,          m_untranslated, 0 );
   MENU(mTools,   E2X("Phone Home"), 0,               phone_home_permit_tip,     m_phone_home_permit, 0 );          //  20.0
   MENU(mTools,   E2X("Show Resources"), 0,           resources_tip,             m_resources, 0 );
   MENU(mTools,   E2X("Appimage Files"), 0,           appimage_files_tip,        m_appimage_files, 0 );             //  20.0
   MENU(mTools,   E2X("Zappcrash Test"), 0,           zappcrash_test_tip,        m_zappcrash_test, 0 ); 

   mHelp = create_popmenu();
   MENU(mHelp,    E2X("User Guide"), 0,               user_guide_tip,            m_help, 0 );
   MENU(mHelp,    E2X("Recent Changes"), 0,           recent_changes_tip,        m_help, 0 );
   MENU(mHelp,    E2X("Edit Functions Overview"), 0,  edit_overview_tip,         m_help, 0 );                       //  20.0
   MENU(mHelp,    E2X("Change Log"), 0,               changelog_tip,             m_help, 0 );
   MENU(mHelp,    E2X("Log File"), 0,                 logfile_tip,               m_help, 0 );
   MENU(mHelp,        "Man Page", 0,                  manpage_tip,               m_help, 0 );                       //  20.0
   MENU(mHelp,    E2X("Command Params"), 0,           command_params_tip,        m_help, 0 );
   MENU(mHelp,    E2X("Translations"), 0,             translations_tip,          m_help, 0 );
   MENU(mHelp,    E2X("Home Page"), 0,                homepage_tip,              m_help, 0 );
   MENU(mHelp,    E2X("License"), 0,                  license_tip,               m_help, 0 );                       //  19.0
   MENU(mHelp,    E2X("Privacy"), 0,                  privacy_tip,               m_help, 0 );                       //  19.1
   MENU(mHelp,    E2X("About"), 0,                    about_tip,                 m_help, 0 );

   //  main menu buttons - must be last

   MENU(0,  E2X("File"),         "menuF.png",         File_tip,            (cbFunc *) popup_menu, (cchar *) mFile);
   MENU(0,  E2X("Gallery"),      "menuG.png",         Gallery_tip,         (cbFunc *) popup_menu, (cchar *) mGallery);
   MENU(0,  E2X("Maps"),         "menuM.png",         Maps_tip,            (cbFunc *) popup_menu, (cchar *) mMap);
   MENU(0,  E2X("Favorites"),    "favorites.png",     favorites_tip,       m_favorites, 0 );
   MENU(0,  E2X("Prev/Next"),    "prev-next.png",     prev_next_tip,       m_prev_next, 0 );
   MENU(0,  E2X("Zoom/±"),       "zoom_menu.png",     zoom_menu_tip,       m_zoom_menu, 0 );
   MENU(0,  E2X("Save"),         "save.png",          save_tip,            m_file_save, 0 );
   MENU(0,  E2X("Meta"),         "meta.png",          meta_tip,            (cbFunc *) popup_menu, (cchar *) mMeta);
   MENU(0,  E2X("Areas"),        "areas.png",         areas_tip,           (cbFunc *) popup_menu, (cchar *) mArea);
   MENU(0,  E2X("Undo/Redo"),    "undo_redo.png",     undo_redo_tip,       m_undo_redo, 0 );
   MENU(0,  E2X("Edit"),         "edit.png",          edit_tip,            (cbFunc *) popup_menu, (cchar *) mEdit);
   MENU(0,  E2X("Enhance"),      "enhance.png",       enhance_tip,         (cbFunc *) popup_menu, (cchar *) mEnhance);
   MENU(0,  E2X("Effects"),      "effects.png",       effects_tip,         (cbFunc *) popup_menu, (cchar *) mEffects);
   MENU(0,  E2X("Warp"),         "warp.png",          warp_tip,            (cbFunc *) popup_menu, (cchar *) mWarp);
   MENU(0,  E2X("Combine"),      "combine.png",       combine_tip,         (cbFunc *) popup_menu, (cchar *) mComb);
   MENU(0,  E2X("Process"),      "process.png",       process_tip,         (cbFunc *) popup_menu, (cchar *) mProc);
   MENU(0,  E2X("Tools"),        "tools.png",         tools_tip,           (cbFunc *) popup_menu, (cchar *) mTools);
   MENU(0,  E2X("Help"),         "help.png",          help_tip,            (cbFunc *) popup_menu, (cchar *) mHelp);
   
   int   Vmenus = Nmenus;                                                        //  visible menu count

   //  internal menus that are not shown in the user menus

   MENU(0, "Copy Pixels 3",  0,  "Copy Pixels Source",  m_copypixels3, 0 );
   MENU(0, "Validate User Guide", 0, "check all user guide links", m_help, 0);                                      //  20.0
   MENU(0, "Autosearch", 0, "Write file list to stdout", m_autosearch, 0);                                          //  20.0
   MENU(0, "dither0", 0, "Roy Lichtenstein Effect", m_dither0, 0);                                                  //  20.0
   MENU(0, "dither1", 0, "pure RGB or black/white dots", m_dither1, 0);                                             //  20.0
   MENU(0, "dither2", 0, "RGB mix with given bit-depth", m_dither2, 0);                                             //  20.0
   MENU(0, "dither3", 0, "custom palette colors", m_dither3, 0);                                                    //  20.0
   
   //  build the menu buttons for the main menu ---------------------------

   float    frgb[3], brgb[3];
   frgb[0] = MFrgb[0] / 256.0;                                                   //  menu font color
   frgb[1] = MFrgb[1] / 256.0;                                                   //  convert range to 0-1
   frgb[2] = MFrgb[2] / 256.0;
   brgb[0] = MBrgb[0] / 256.0;                                                   //  menu background color
   brgb[1] = MBrgb[1] / 256.0;                                                   //  convert range to 0-1
   brgb[2] = MBrgb[2] / 256.0;

   Vmenu *Xvm = Vmenu_new(MWmenu,frgb,brgb);                                     //  create main menu

   int   siz = iconsize;                                                         //  user preferences parameter

   for (me = 0; me < Vmenus; me++)
   {
      if (menutab[me].topmenu)                                                   //  submenu within top menu
         add_popmenu_item(menutab[me].topmenu, menutab[me].menu,
                menutab[me].func, menutab[me].arg, menutab[me].desc);

      else                                                                       //  top menu (button)
      {
         if (strmatch(menu_style,"icons")) {                                     //  icons only
            if (menutab[me].icon)
               Vmenu_add(Xvm, 0, menutab[me].icon,siz,siz,menutab[me].desc,
                                 menutab[me].func, menutab[me].arg);
            else                                                                 //  no icon, use menu text
               Vmenu_add(Xvm, menutab[me].menu, 0, 0, 0, menutab[me].desc,
                                 menutab[me].func, menutab[me].arg);
         }
         
         else if (strmatch(menu_style,"text")) {                                 //  text only
            Vmenu_add(Xvm, menutab[me].menu, 0, 0, 0, menutab[me].desc,
                        menutab[me].func, menutab[me].arg);
         }
         
         else                                                                    //  icons + menu text
            Vmenu_add(Xvm, menutab[me].menu, menutab[me].icon, siz, siz,
                        menutab[me].desc, menutab[me].func, menutab[me].arg);
      }
   }
   
   Vmenu_add_RMfunc(Xvm, 0, m_viewmode, "F");                                    //  add right-mouse function           19.0
   Vmenu_add_RMfunc(Xvm, 1, m_viewmode, "G");                                    //    for top 3 menu buttons
   Vmenu_add_RMfunc(Xvm, 2, m_viewmode, "?");


   //  build table of eligible menus for KB shortcut assignment -----------------

   #define KBshort(_menu, _func, _arg)                               \
      me = Nkbsf++;                                                  \
      if (me >= maxkbsf) zappcrash("maxkbs exceeded");               \
      kbsftab[me].menu = _menu;                                      \
      kbsftab[me].func = _func;                                      \
      kbsftab[me].arg = _arg;

   Nkbsf = 0;

   //       menu                 called function        arg
   KBshort("Adjust RGB",         m_adjust_RGB,           0     );
   KBshort("Adjust HSL",         m_adjust_HSL,           0     );
   KBshort("All Folders",        m_alldirs,              0     );
   KBshort("Blank Window",       m_blank_window,         0     );
   KBshort("Blur",               m_blur,                 0     );
   KBshort("Bookmarks",          m_bookmarks,            0     );
   KBshort("Brite Dist",         m_brite_dist,           0     );
   KBshort("Captions",           m_meta_captions,       "x"    );
   KBshort("Change Language",    m_change_lang,          0     );
   KBshort("Color Depth",        m_color_depth,          0     );
   KBshort("Color Mode",         m_color_mode,           0     );
   KBshort("Copy/Move",          m_copy_move,            0     );
   KBshort("Copy to Clipboard",  m_copyto_clip,          0     );
   KBshort("Copy to Desktop",    m_copyto_desktop,       0     );
   KBshort("Current Album",      m_current_album,        0     );                //  20.0
   KBshort("Cycle 2",            m_cycle2files,          0     );
   KBshort("Cycle 3",            m_cycle3files,          0     );
   KBshort("Delete Meta",        m_meta_delete,          0     );
   KBshort("Delete/Trash",       m_delete_trash,         0     );
   KBshort("Denoise",            m_denoise,              0     );
   KBshort("Edit Any Meta",      m_meta_edit_any,        0     );
   KBshort("Edit Meta",          m_meta_edit_main,       0     );
   KBshort("File View",          m_viewmode,            "F"    );
   KBshort("Flatten",            m_flatten,              0     );
   KBshort("Gallery",            m_viewmode,            "G"    );
   KBshort("Global Retx",        m_gretinex,             0     );
   KBshort("Gradients",          m_gradients,            0     );
   KBshort("Grid Lines",         m_gridlines,            0     );
   KBshort("KB Shortcuts",       m_KBshortcuts,          0     );
   KBshort("Line Color",         m_line_color,           0     );
   KBshort("List View",          m_listview,             0     );                //  20.0
   KBshort("Magnify Image",      m_magnify,              0     );
   KBshort("Manage Albums",      m_manage_albums,        0     );
   KBshort("Map View",           m_viewmode,            "?"    );
   KBshort("Markup",             m_markup,               0     );
   KBshort("Meta View",          m_metaview,             0     );                //  20.0
   KBshort("Mirror",             m_mirror,               0     );
   KBshort("Newest",             m_newfiles,             0     );
   KBshort("New Version",        m_file_save_version,    0     );
   KBshort("Places/Dates",       m_meta_places_dates,    0     );
   KBshort("Print",              m_print,                0     );
   KBshort("Print Calibrated",   m_print_calibrated,     0     );
   KBshort("Raw Therapee",       m_rawtherapee,          0     );
   KBshort("Recent",             m_recentfiles,          0     );
   KBshort("Red Eyes",           m_redeyes ,             0     );
   KBshort("Redo",               m_redo,                 0     );
   KBshort("Rename",             m_rename,               0     );
   KBshort("RGB Distribution",   m_RGB_dist,             0     );
   KBshort("Permissions",        m_permissions,          0     );                //  20.0
   KBshort("Replace",            m_file_save_replace,    0     );
   KBshort("Resize",             m_resize,               0     );
   KBshort("Retouch",            m_retouch,              0     );
   KBshort("Save",               m_file_save,            0     );
   KBshort("Search",             m_search_images,        0     );
   KBshort("Select Files",       m_select_files,         0     );                //  20.0
   KBshort("Sharpen",            m_sharpen,              0     );
   KBshort("Show Hidden",        m_show_hidden,          0     );
   KBshort("Show on Map",        m_netmap_zoomin,        0     ); 
   KBshort("Source Folder",      m_source_folder,        0     );
   KBshort("Timeline",           m_meta_timeline,        0     );
   KBshort("Trim/Rotate",        m_trim_rotate,          0     );
   KBshort("Undo",               m_undo,                 0     );
   KBshort("Upright",            m_upright,              0     );
   KBshort("Preferences",        m_preferences,          0     );
   KBshort("View Meta",          m_meta_view_short,      0     );
   KBshort("View 360° Pano",     m_view360,              0     );
   KBshort("Voodoo 1",           m_voodoo1,              0     );
   KBshort("Voodoo 2",           m_voodoo2,              0     );
   KBshort("Zonal Retx",         m_zretinex,             0     );
   //  zdialog completion buttons that can have KB shortcuts
   KBshort("Done",               0,                      0     );
   KBshort("Cancel",             0,                      0     );
   KBshort("Apply",              0,                      0     );
   KBshort("Reset",              0,                      0     );
   
   //  build right-click popup menus --------------------------------------------

   cchar    *menupopimage = E2X("Popup Image");
   cchar    *menumeta1 = E2X("View Meta");
   cchar    *menumeta1A = E2X("View All Meta");
   cchar    *menumeta2 = E2X("Edit Meta");
   cchar    *menumeta3 = E2X("Edit Any Meta");
   cchar    *menurename = E2X("Rename");
   cchar    *menupermissions = E2X("Permissions");                               //  20.0
   cchar    *menucopymove = E2X("Copy/Move");
   cchar    *menucopytodesktop = E2X("Copy to Desktop");
   cchar    *menucopytoclip = E2X("Copy to Clipboard");
   cchar    *menualbumaddselfiles = E2X("Add Selected Files Here");
   cchar    *menualbumaddcurrfile = E2X("Add Current File Here");
   cchar    *menuremovefromalbum = E2X("Remove from Album");
   cchar    *menutrimrotate = E2X("Trim/Rotate");
   cchar    *menuresize = E2X("Resize");
   cchar    *menuupright = E2X("Upright");
   cchar    *menuvoodoo1 = "Voodoo 1";
   cchar    *menuvoodoo2 = "Voodoo 2";
   cchar    *menuretouch = E2X("Retouch");
   cchar    *menubritedist = E2X("Brite Dist");
   cchar    *menuflatten = E2X("Flatten");
   cchar    *menugradients = E2X("Gradients");
   cchar    *menuselect = E2X("Select Area");
   cchar    *menuopenraw = E2X("Raw Therapee");
   cchar    *menunetzoom = E2X("Show on Map");
   cchar    *menudeltrash = E2X("Delete/Trash");

   popmenu_image = create_popmenu();                                             //  popup menu for image files
   add_popmenu_item(popmenu_image,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_image,menumeta1A,popup_menufunc,"view all metadata");
   add_popmenu_item(popmenu_image,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_image,menumeta3,popup_menufunc,"edit any metadata");
   add_popmenu_item(popmenu_image,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_image,menupermissions,popup_menufunc,"permissions");
   add_popmenu_item(popmenu_image,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_image,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_image,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_image,menuupright,popup_menufunc,"upright");
   add_popmenu_item(popmenu_image,menutrimrotate,popup_menufunc,"trim/rotate");
   add_popmenu_item(popmenu_image,menuresize,popup_menufunc,"resize");
   add_popmenu_item(popmenu_image,menuvoodoo1,popup_menufunc,"voodoo1");
   add_popmenu_item(popmenu_image,menuvoodoo2,popup_menufunc,"voodoo2");
   add_popmenu_item(popmenu_image,menuretouch,popup_menufunc,"retouch");
   add_popmenu_item(popmenu_image,menubritedist,popup_menufunc,"brite dist");
   add_popmenu_item(popmenu_image,menuflatten,popup_menufunc,"flatten");
   add_popmenu_item(popmenu_image,menugradients,popup_menufunc,"gradients");
   add_popmenu_item(popmenu_image,menuselect,popup_menufunc,"select");
   add_popmenu_item(popmenu_image,menunetzoom,popup_menufunc,"netmap_zoomin");
   add_popmenu_item(popmenu_image,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_raw = create_popmenu();                                               //  popup menu for RAW files
   add_popmenu_item(popmenu_raw,menuopenraw,m_rawtherapee,0);
   add_popmenu_item(popmenu_raw,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_raw,menumeta1A,popup_menufunc,"view all metadata");
   add_popmenu_item(popmenu_raw,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_raw,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_raw,menupermissions,popup_menufunc,"permissions");
   add_popmenu_item(popmenu_raw,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_raw,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_raw,menunetzoom,popup_menufunc,"netmap_zoomin");
   add_popmenu_item(popmenu_raw,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_video = create_popmenu();                                             //  popup menu for VIDEO files
   add_popmenu_item(popmenu_video,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_video,menumeta1A,popup_menufunc,"view all metadata");
   add_popmenu_item(popmenu_video,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_video,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_video,menupermissions,popup_menufunc,"permissions");
   add_popmenu_item(popmenu_video,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_video,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_video,menunetzoom,popup_menufunc,"netmap_zoomin");
   add_popmenu_item(popmenu_video,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_thumb = create_popmenu();                                             //  gallery thumbnail popup menu
   add_popmenu_item(popmenu_thumb,menupopimage,popup_menufunc,"popimage");
   add_popmenu_item(popmenu_thumb,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_thumb,menumeta1A,popup_menufunc,"view all metadata");
   add_popmenu_item(popmenu_thumb,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_thumb,menumeta3,popup_menufunc,"edit any metadata");
   add_popmenu_item(popmenu_thumb,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_thumb,menupermissions,popup_menufunc,"permissions");
   add_popmenu_item(popmenu_thumb,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_thumb,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_thumb,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_thumb,menuupright,popup_menufunc,"upright");
   add_popmenu_item(popmenu_thumb,menunetzoom,popup_menufunc,"netmap_zoomin");
   add_popmenu_item(popmenu_thumb,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_album = create_popmenu();                                             //  album thumbnail popup menu
   add_popmenu_item(popmenu_album,menupopimage,popup_menufunc,"popimage");
   add_popmenu_item(popmenu_album,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_album,menumeta1A,popup_menufunc,"view all metadata");
   add_popmenu_item(popmenu_album,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_album,menumeta3,popup_menufunc,"edit any metadata");
   add_popmenu_item(popmenu_album,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_album,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_album,menualbumaddselfiles,popup_menufunc,"albumaddselfiles");
   add_popmenu_item(popmenu_album,menualbumaddcurrfile,popup_menufunc,"albumaddcurrfile");
   add_popmenu_item(popmenu_album,menuremovefromalbum,popup_menufunc,"removefromalbum");

   return;
}


//  right-click popup menu response function

void popup_menufunc(GtkWidget *, cchar *menu)
{
   if (strmatch(menu,"popimage")) gallery_popimage();                            //  funcs for main and gallery windows
   if (strmatch(menu,"upright")) m_upright(0,0);                                 //  20.0
   if (strmatch(menu,"view metadata")) meta_view(1);
   if (strmatch(menu,"view all metadata")) meta_view(2);
   if (strmatch(menu,"edit metadata")) m_meta_edit_main(0,0);                    //  these use clicked_file if defined,
   if (strmatch(menu,"edit any metadata")) m_meta_edit_any(0,0);                 //    else they use curr_file.
   if (strmatch(menu,"rename")) m_rename(0,0);
   if (strmatch(menu,"permissions")) m_permissions(0,0);                         //  20.0
   if (strmatch(menu,"copymove")) m_copy_move(0,0);
   if (strmatch(menu,"copytodesktop")) m_copyto_desktop(0,0);
   if (strmatch(menu,"delete/trash")) m_delete_trash(0,0);
   if (strmatch(menu,"netmap_zoomin")) m_netmap_zoomin(0,0);
   if (strmatch(menu,"copytoclip")) m_copyto_clip(0,0);

   if (strmatch(menu,"removefromalbum")) album_removefile(clicked_posn);         //  funcs for album gallery depend
   if (strmatch(menu,"albumaddselfiles")) album_addselfiles(2);                  //    on 'clicked_posn' being set
   if (strmatch(menu,"albumaddcurrfile")) album_addcurrfile(2);

   if (strmatch(menu,"trim/rotate")) m_trim_rotate(0,0);                         //  functions using curr_file only
   if (strmatch(menu,"resize")) m_resize(0,0);                                   //  (not for gallery/thumbnail click)
   if (strmatch(menu,"voodoo1")) m_voodoo1(0,0);
   if (strmatch(menu,"voodoo2")) m_voodoo2(0,0);
   if (strmatch(menu,"retouch")) m_retouch(0,0);
   if (strmatch(menu,"brite dist")) m_brite_dist(0,0);
   if (strmatch(menu,"flatten")) m_flatten(0,0);
   if (strmatch(menu,"gradients")) m_gradients(0,0);
   if (strmatch(menu,"select")) m_select(0,0);

   return;
}


//  main window mouse right-click popup menu

void image_Rclick_popup()
{
   int      ftype;
   
   if (! curr_file) return;
   ftype = image_file_type(curr_file);
   if (ftype == IMAGE) popup_menu(Mwin,popmenu_image);
   if (ftype == RAW) popup_menu(Mwin,popmenu_raw);
   if (ftype == VIDEO) popup_menu(Mwin,popmenu_video);
   return;
}


//  gallery thumbnail mouse left-click function
//  open the clicked file in view mode F

void gallery_Lclick_func(int Nth)
{
   char     *file;
   int      err;

   if (clicked_file) {                                                           //  lose memory of clicked thumbnail
      zfree(clicked_file);
      clicked_file = 0;
   }
   if (checkpend("busy block mods")) return;
   file = gallery(0,"get",Nth);
   if (! file) return;
   err = f_open(file,Nth,0,1);                                                   //  clicked file >> current file
   zfree(file);
   if (! err) m_viewmode(0,"F");
   return;
}


//  gallery thumbnail mouse right-click popup menu

void gallery_Rclick_popup(int Nth)
{
   FTYPE    ftype;

   clicked_posn = Nth;                                                           //  clicked gallery position (0 base)
   clicked_file = gallery(0,"get",Nth);                                          //  clicked_file is subject for zfree()
   if (! clicked_file) return;

   ftype = image_file_type(clicked_file);

   if (navi::gallerytype == ALBUM)
      popup_menu(Mwin,popmenu_album);

   else if (ftype == IMAGE)
      popup_menu(Mwin,popmenu_thumb);

   else if (ftype == RAW)
      popup_menu(Mwin,popmenu_raw);

   else if (ftype == VIDEO) 
      popup_menu(Mwin,popmenu_video);

   return;
}


/********************************************************************************/

//  set window view mode, F/G/M/W

void m_viewmode(GtkWidget *, cchar *fgwm)
{
   static char    lastWM[4] = "M";
   
   if (FGWM == *fgwm) return;                                                    //  no change

   if (*fgwm == '0')                                                             //  set no view mode (blank window)    19.0
   {
      gtk_widget_hide(Fhbox);
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Whbox);
      gtk_widget_hide(Mhbox);
      FGWM = '0';
      PFGWM = '0';                                                               //  remember last F/G view

      Cstate = 0;                                                                //  no F/W image drawing area
      Cdrawin = 0;
      gdkwin = 0;

      if (zd_deltrash) m_delete_trash(0,0);                                      //  set target file in active dialog
      if (zd_copymove) m_copy_move(0,0);
      if (zd_metaview) meta_view(0);
      if (zd_rename) m_rename(0,0);
      if (zd_permissions) m_permissions(0,0);                                    //  20.0
   }

   if (*fgwm == 'F')                                                             //  set F view mode for image file
   {
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Whbox);
      gtk_widget_hide(Mhbox);
      gtk_widget_show_all(Fhbox);
      FGWM = 'F';
      PFGWM = 'F';                                                               //  remember last F/G view

      set_mwin_title();

      Cstate = &Fstate;                                                          //  set drawing area
      Cdrawin = Fdrawin;
      gdkwin = gtk_widget_get_window(Fdrawin);                                   //  GDK window

      if (zd_deltrash) m_delete_trash(0,0);                                      //  set target file in active dialog
      if (zd_copymove) m_copy_move(0,0);
      if (zd_metaview) meta_view(0);
      if (zd_rename) m_rename(0,0);
      if (zd_permissions) m_permissions(0,0);                                    //  20.0
   }

   if (*fgwm == 'G')                                                             //  set G view mode for thumbnail gallery
   {
      gtk_widget_hide(Fhbox);
      gtk_widget_hide(Whbox);
      gtk_widget_hide(Mhbox);
      gtk_widget_show_all(Ghbox);
      FGWM = 'G';
      PFGWM = 'G';                                                               //  remember last F/G view

      Cstate = 0;                                                                //  no F/W image drawing area
      Cdrawin = 0;
      gdkwin = 0;

      if (curr_file) gallery(curr_file,"paint",0);                               //  set gallery posn. at curr. file
      else gallery(0,"paint",-1);                                                //  else leave unchanged
   }

   if (*fgwm == 'M')                                                             //  set M view mode for net maps
   {
      if (CEF) return;                                                           //  don't interrupt edit func.
      
      gtk_widget_hide(Fhbox);                                  
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Whbox);
      gtk_widget_show_all(Mhbox);
      FGWM = 'M';
      lastWM[0] = 'M';

      Cstate = 0;                                                                //  no F/W image drawing area
      Cdrawin = 0;
      gdkwin = 0;
      
      m_load_netmap(0,"init");                                                   //  load net initial map
      gtk_window_set_title(MWIN,E2X("Fotoxx   Image Locations"));                //  window title
   }

   if (*fgwm == 'W')                                                             //  set W view mode for file maps
   {
      if (CEF) return;                                                           //  don't interrupt edit func.

      gtk_widget_hide(Fhbox);                                  
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Mhbox);
      gtk_widget_show_all(Whbox);
      FGWM = 'W';
      lastWM[0] = 'W';

      Cstate = &Wstate;                                                          //  set drawing area
      Cdrawin = Wdrawin;
      gdkwin = gtk_widget_get_window(Wdrawin);                                   //  GDK window

      if (! Wstate.fpxb) m_load_filemap(0,"default");                            //  no map loaded, load default map 
      gtk_window_set_title(MWIN,E2X("Fotoxx   Image Locations"));                //  window title
      Fpaintnow();
   }
   
   if (*fgwm == '?') m_viewmode(0,lastWM);                                       //  use last set W/M mode              19.0

   return;
}


/********************************************************************************/

//  favorites menu - popup graphic menu with user's favorites

void m_favorites(GtkWidget *, cchar *)
{
   void  favorites_callback(cchar *menu);
   char  menuconfigfile[200];

   F1_help_topic = "favorites";
   snprintf(menuconfigfile,200,"%s/menu-config",favorites_folder);
   Gmenuz(Mwin,E2X("Favorites"),menuconfigfile,favorites_callback);
   return;
}


//  response function for clicked menu
//  a menu function is called as from the text menus

void favorites_callback(cchar *menu)
{
   int      ii;

   if (! menu) return;
   if (strmatchcase(menu,"quit")) return;

   if (strmatchcase(menu,"help")) {                                              //  special case
      showz_docfile(Mwin,"userguide","favorites");                               //  'help' menu in favorites menu
      return;
   }
   
   for (ii = 0; ii < Nmenus; ii++) {                                             //  search for menu function
      if (! menutab[ii].menu) continue;
      if (! menutab[ii].topmenu) continue;
      if (strmatchcase(menu,menutab[ii].menu)) break;
      if (strmatchcase(E2X(menu),menutab[ii].menu)) break;
   }
   
   if (ii == Nmenus) {
      zmessageACK(Mwin,E2X("invalid menu name: %s"),menu);
      return;
   }
   
   menutab[ii].func(0,menu);                                                     //  start the function
   return;
}


