#! /bin/bash
# run from fotoxx source directory  /.../fotoxx-N.N

cp -L /usr/lib/x86_64-linux-gnu/libjasper.so.1  AppDir/usr/lib
cp -L /usr/lib/x86_64-linux-gnu/libjpeg.so.8  AppDir/usr/lib
cp -L /usr/lib/x86_64-linux-gnu/libpng12.so.0  AppDir/usr/lib
cp -L /usr/lib/x86_64-linux-gnu/libraw.so.15.0.0  AppDir/usr/lib/libraw.so.15
cp -L /usr/lib/x86_64-linux-gnu/libchamplain-0.12.so.0  AppDir/usr/lib
cp -L /usr/lib/x86_64-linux-gnu/libchamplain-gtk-0.12.so.0  AppDir/usr/lib

mkdir -p usr/share/mime                                  ## pixbuf loader data
cp -L -R /usr/share/mime/image  AppDir/usr/share/mime
cp -L -R /usr/share/mime/magic  AppDir/usr/share/mime
cp -L -R /usr/share/mime/mime.cache  AppDir/usr/share/mime

mkdir -p usr/share/perl5                                 ## exiftool
cp -L -R /usr/bin/exiftool  AppDir/usr/bin
cp -L -R /usr/share/libimage-exiftool-perl  AppDir/usr/share
cp -L -R /usr/share/perl5/Image  AppDir/usr/share/perl5
cp -L -R /usr/share/perl5/File  AppDir/usr/share/perl5

## needed only for debug using -fsanitize=address
cp -L /usr/lib/x86_64-linux-gnu/libasan.so.2.0.0  AppDir/usr/lib/libasan.so.2 

## not needed
## cp -L -R /usr/lib/x86_64-linux-gnu/gdk-pixbuf-2.0  usr/lib
## cp -L /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2  usr/lib
## cp -L /lib/x86_64-linux-gnu/libc.so.6  usr/lib


